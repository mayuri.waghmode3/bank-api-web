import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositOpComponent } from './deposit-op.component';

describe('DepositOpComponent', () => {
  let component: DepositOpComponent;
  let fixture: ComponentFixture<DepositOpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositOpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositOpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
