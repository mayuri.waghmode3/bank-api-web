import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveAccOpComponent } from './remove-acc-op.component';

describe('RemoveAccOpComponent', () => {
  let component: RemoveAccOpComponent;
  let fixture: ComponentFixture<RemoveAccOpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveAccOpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveAccOpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
