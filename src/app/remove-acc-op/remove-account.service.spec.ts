import { TestBed } from '@angular/core/testing';

import { RemoveAccountService } from './remove-account.service';

describe('RemoveAccountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemoveAccountService = TestBed.get(RemoveAccountService);
    expect(service).toBeTruthy();
  });
});
