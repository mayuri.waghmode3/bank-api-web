import { Component, OnInit } from '@angular/core';
import { RemoveAccountService } from './remove-account.service';
import { ResponseBody } from '../responseBody';
import { Account } from '../main-menu/Account';
import { Customer } from '../customer';
import { AccountType } from '../main-menu/AccountType';
import { MainServiceService } from '../main-menu/main-service.service';

@Component({
  selector: 'app-remove-acc-op',
  templateUrl: './remove-acc-op.component.html',
  styleUrls: ['../main-menu/main-menu.component.css']
})
export class RemoveAccOpComponent implements OnInit {
  selectedAccount : string;
  success : boolean;
  accNo : string;
  resp = new ResponseBody();
  account = new Account();
  customer = new Customer();
  accounts: Account[];
  constructor(private removeAccountService: RemoveAccountService,private mainServiceService : MainServiceService) {
    this.customer = JSON.parse(localStorage.getItem('currentUser'));
    this.getAccounts();
   }

  ngOnInit() {
  }
 removeAccount(){
  this.accNo = this.selectedAccount.split("::")[0];
    this.removeAccountService.removeAccount(this.accNo).then(data =>
      {this.resp = data;
        this.getAccounts();
        this.success = this.resp.code=='200' ? true : false;});
    
  }
  getAccounts() : void{
    this.mainServiceService.getAccounts(this.customer.username)
          .subscribe(accData => 
            {this.accounts = accData; this.selectedAccount = this.accounts[0].accNo+"::"+this.accounts[0].accountType.type;}
            ,(error)=>{console.log(error)
          });
  }

}
