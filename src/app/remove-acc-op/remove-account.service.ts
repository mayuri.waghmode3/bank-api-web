import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RemoveAccountService {
  url : string;
  constructor(private http: HttpClient) { }
  async removeAccount(accNo : string):Promise<any> {
    this.url =  "http://localhost:8080/deleteAcc/"+accNo;
    return await this.http.delete(this.url).toPromise();
  }
}
