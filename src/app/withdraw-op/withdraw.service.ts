import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WithdrawService {
  url : string;
  constructor(private http : HttpClient) { }

  async withdraw(accNo : string,amount : number) : Promise<any>{
    this.url =  "http://localhost:8080/withdraw?amount="+amount+"&accNo="+accNo;
      
    return await this.http.put(this.url, {observe: 'response'}).toPromise();

  }
}
