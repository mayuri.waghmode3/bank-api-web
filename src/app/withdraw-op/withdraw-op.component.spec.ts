import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithdrawOpComponent } from './withdraw-op.component';

describe('WithdrawOpComponent', () => {
  let component: WithdrawOpComponent;
  let fixture: ComponentFixture<WithdrawOpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithdrawOpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawOpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
