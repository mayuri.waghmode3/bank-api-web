import { Component, OnInit } from '@angular/core';
import { AuthService } from '../login/auth.service';
import { Observable } from 'rxjs';
import { Customer } from '../customer';
@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  constructor(private authService:AuthService) {
   
   }

  ngOnInit() {
  }
  onLogout() {
    console.log("inside onLogout()");
    localStorage.clear();
    this.authService.logout();
  }
}
