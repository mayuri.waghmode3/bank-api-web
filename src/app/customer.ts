export class Customer {
    id: number;
    username: string;
    password: string;
    email: string;
    mobileNo: string;
    firstName: string;
    lastName: string;
    address: string;
    city: string;
    zip: string;
    state: string;
}