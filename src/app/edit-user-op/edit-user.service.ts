import { Injectable } from '@angular/core';
import { Customer } from '../customer';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class EditUserService {

  constructor( private http: HttpClient) { }
  url : string;
 
  editUser(customer : Customer) : Observable<any>{
   
    let body = JSON.stringify(customer);
    console.log(body);
    this.url =  "http://localhost:8080/profile";
    return  this.http.put(this.url, body,httpOptions);
  
  }
  
}
