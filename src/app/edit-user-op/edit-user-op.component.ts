import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { ResponseBody } from '../responseBody';
import { EditUserService } from './edit-user.service';

@Component({
  selector: 'app-edit-user-op',
  templateUrl: './edit-user-op.component.html',
  styleUrls: ['./edit-user-op.component.css']
})
export class EditUserOpComponent implements OnInit {
  customer : Customer;
  resp = new ResponseBody();
  success : boolean;
  message : string;

  constructor(private editUserService : EditUserService) { 
    this.customer = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    
  }

  saveUser(): void{ 
    if(this.customer && (Object.keys(this.customer).length === 0)){
      this.message = "please enter all user information";
         
      this.success = false;
      return;
     }
    if(this.customer.address.length === 0 || this.customer.city.length === 0 || 
        this.customer.email.length === 0 || this.customer.firstName.length === 0 || 
        this.customer.lastName.length === 0 || this.customer.mobileNo.length === 0 || 
        this.customer.password.length === 0 || this.customer.state.length === 0 ||
        this.customer.username.length === 0 || this.customer.zip.length === 0 ){
          this.message = "please enter all user information";
          console.log("hi");
          this.success = false;
          return;
    }else{
      this.editUserService.editUser(this.customer).subscribe(resp => {
      this.resp = resp;
      this.success = this.resp.code=='200' ? true : false;
      this.message = this.resp.message;
      })
      localStorage.setItem('currentUser', JSON.stringify(this.customer));
  }
}

}
