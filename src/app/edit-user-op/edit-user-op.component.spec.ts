import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserOpComponent } from './edit-user-op.component';

describe('EditUserOpComponent', () => {
  let component: EditUserOpComponent;
  let fixture: ComponentFixture<EditUserOpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUserOpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUserOpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
