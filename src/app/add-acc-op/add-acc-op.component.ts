import { Component, OnInit } from '@angular/core';
import { Account } from '../main-menu/Account';
import { AccountType } from '../main-menu/AccountType';
import { ResponseBody } from '../responseBody';
import { Customer } from '../customer';
import { AddAccountService } from './add-account.service';

@Component({
  selector: 'app-add-acc-op',
  templateUrl: './add-acc-op.component.html',
  styleUrls: ['../main-menu/main-menu.component.css']
})
export class AddAccOpComponent implements OnInit {
 success : boolean;
 accNo : string;
 resp = new ResponseBody();
 account = new Account();
 customer = new Customer();
 accountTypes: AccountType[];
 selectedAccountType : string;

  constructor(private addAccountService : AddAccountService) {
    this.customer = JSON.parse(localStorage.getItem('currentUser'));
    this.getAccountTypes();
   }

  ngOnInit() {
  }
  addAccount(){
    
      this.account.customer = this.customer;
      this.account.accNo = this.accNo;
      this.account.balance=0;
      console.log(this.selectedAccountType);
      var i :number;
    for( i = 0; i<this.accountTypes.length;i++)
        if(this.accountTypes[i].type.match(this.selectedAccountType))
          this.account.accountType = this.accountTypes[i];
    
    console.log(this.account);
    this.addAccountService.addAccount(this.account).then((rs : ResponseBody)=>{
      this.resp = rs; this.success = this.resp.code=='200' ? true : false;});
  }
  getAccountTypes() {
    this.addAccountService.getAccountType(this.selectedAccountType).then((rs : AccountType[])=>
    {this.accountTypes = rs; this.selectedAccountType =  this.accountTypes[0].type;});
  }
}
