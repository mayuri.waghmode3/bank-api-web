import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AccountType } from '../main-menu/AccountType';
import { Account } from '../main-menu/Account';
import { catchError } from 'rxjs/operators';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class AddAccountService {
  url : string;

  constructor(private http: HttpClient) {

   }

   async addAccount(account : Account):Promise<any> {
    this.url =  "http://localhost:8080/addAcc/";
    let body = JSON.stringify(account);
    return await this.http.post(this.url, body,httpOptions).toPromise();
  }

  async getAccountType(type : String): Promise<AccountType[]>{
    this.url =  "http://localhost:8080/getAccountTypes/";
    return await this.http.get<AccountType[]>(this.url).toPromise();
  }  
  
  private handleError(error : Response){
    return Observable.throw(error);
  }

}
