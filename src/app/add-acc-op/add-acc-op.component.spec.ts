import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccOpComponent } from './add-acc-op.component';

describe('AddAccOpComponent', () => {
  let component: AddAccOpComponent;
  let fixture: ComponentFixture<AddAccOpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAccOpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAccOpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
