import { Component, OnInit } from '@angular/core';
import { ResponseBody } from '../responseBody';
import { Account } from '../main-menu/Account';
import { Customer } from '../customer';
import { MainServiceService } from '../main-menu/main-service.service';
import { TransferService } from './transfer.service';

@Component({
  selector: 'app-transfer-op',
  templateUrl: './transfer-op.component.html',
  styleUrls: ['../main-menu/main-menu.component.css']
})
export class TransferOpComponent implements OnInit {
  selectedAccount : string;
  success : boolean;
  accNo : string;
  accTo : string;
  amount : number;
  resp = new ResponseBody();
  account = new Account();
  customer = new Customer();
  accounts: Account[];
 
  constructor(private mainServiceService : MainServiceService, private transferService :TransferService) { 
    this.customer = JSON.parse(localStorage.getItem('currentUser'));
    this.getAccounts();
  }

  ngOnInit() {
  }
  
  transfer() : void{
    this.accNo = this.selectedAccount.split("::")[0];
   
    this.transferService.transfer(this.accNo,this.accTo,this.amount).then(data =>
      {this.resp = data;
        this.getAccounts();
        this.success = this.resp.code=='200' ? true : false;});
    
  }
  getAccounts() : void{
    this.mainServiceService.getAccounts(this.customer.username)
          .subscribe(accData => 
            {this.accounts = accData; this.selectedAccount = this.accounts[0].accNo+"::"+this.accounts[0].accountType.type;}
            ,(error)=>{console.log(error)
          });
  }

}
