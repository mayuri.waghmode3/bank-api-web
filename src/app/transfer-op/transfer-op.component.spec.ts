import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferOpComponent } from './transfer-op.component';

describe('TransferOpComponent', () => {
  let component: TransferOpComponent;
  let fixture: ComponentFixture<TransferOpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferOpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferOpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
