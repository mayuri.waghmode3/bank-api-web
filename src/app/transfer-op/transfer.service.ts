import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransferService {

  url : string;
  constructor(private http : HttpClient) { }

  async transfer(fromAccNo : string,toAccNo : string, amount : number) : Promise<any>{
    this.url =  "http://localhost:8080/transfer?amount="+amount+"&fromAccNo="+fromAccNo+"&toAccNo="+toAccNo;
      
    return await this.http.put(this.url, {observe: 'response'}).toPromise();

  }
  
}
