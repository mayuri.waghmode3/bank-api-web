import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { Customer } from '../customer';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  

  constructor(private router: Router) { }
 
  
  login(data : any,customer : Customer) : string {
    if(customer == null)
      return "Incorrect username or password";  
    if(customer.password == data.password && customer.username == data.username ){
      localStorage.setItem('currentUser', JSON.stringify(customer));
     
      this.router.navigate(['/main/getBal']);
    }else
      return "Incorrect username or password";
              
  }
  logout() {
    this.router.navigate(['/login']);
  }
  
}
