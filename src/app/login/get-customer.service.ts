import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Customer } from '../customer';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class GetCustomerService {

  constructor( private http: HttpClient) { }
  url : string;
  getCustomer(username : string, password : string) : Promise<Customer> {
    this.url =  "http://localhost:8080/login/"+username+"/"+password;
    return this.http.get<Customer>(this.url).toPromise();
  }
}
