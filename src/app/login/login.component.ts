import { Component, OnInit } from '@angular/core';
import { GetCustomerService } from './get-customer.service';
import { AuthService } from './auth.service';
import { BehaviorSubject } from 'rxjs';
import { Customer } from '../customer';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ GetCustomerService,AuthService ]
})
export class LoginComponent implements OnInit {

  message : string;
  success: boolean;
  constructor(private getCustomerService : GetCustomerService,private authService : AuthService ) {
   
   }
  ngOnInit() {
  
  }
  
  customer: Customer; 
  onClickSubmit(data: any): void{  
    if(data.username == '' || data.password == ''){
      this.message = "username or password cannot be empty";
      this.success = false;
    }else{
     this.getCustomerService.getCustomer(data.username,data.password)
              .then(customerData => {this.customer = customerData;
                this.message = this.authService.login(data,this.customer);
                this.success = this.message.length<=0 ? true : false; }); 
    }
  }
 

  

  
}