import { Component, OnInit } from '@angular/core';
import { ViewTransactionService } from './view-transaction.service';
import { Customer } from '../customer';
import { Log } from './log';

@Component({
  selector: 'app-view-transaction-op',
  templateUrl: './view-transaction-op.component.html',
  styleUrls: ['../main-menu/main-menu.component.css']
})
export class ViewTransactionOpComponent implements OnInit {
  customer :Customer;
  logs : Log[]; 
  constructor(private viewTransactionService:ViewTransactionService) {
    this.customer = JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
    this.summary();
  }
  summary(): void{ 
    this.viewTransactionService.summary(this.customer.username)
              .subscribe(customerData =>
                 {this.logs = customerData;
                  console.log(this.logs);
                 },
                 (error)=>{console.log(error)
              });
/*
  for(int i =0; i<report.size();i++) {
        	report.get(i).setTimestamp(report.get(i).getTimestamp().toLocaleString());
        }
*/
  } 
}