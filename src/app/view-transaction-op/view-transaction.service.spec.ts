import { TestBed } from '@angular/core/testing';

import { ViewTransactionService } from './view-transaction.service';

describe('ViewTransactionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewTransactionService = TestBed.get(ViewTransactionService);
    expect(service).toBeTruthy();
  });
});
