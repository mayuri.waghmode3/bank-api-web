import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTransactionOpComponent } from './view-transaction-op.component';

describe('ViewTransactionOpComponent', () => {
  let component: ViewTransactionOpComponent;
  let fixture: ComponentFixture<ViewTransactionOpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTransactionOpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTransactionOpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
