import { Customer } from '../customer';

 export interface Log{
   logId: number;
   timestamp : Date;
   operation : string;
   customer : Customer;
 }
 