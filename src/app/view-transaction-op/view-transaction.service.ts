import { Injectable } from '@angular/core';
import { Customer } from '../customer';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ViewTransactionService {

  constructor( private http: HttpClient) { }
  url : string;
 
  summary(username : string) : Observable<any>{
    
    this.url =  "http://localhost:8080/summary/"+username;
    return this.http.get(this.url);
  
  }
}
