import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { AuthService } from '../login/auth.service';

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.css']
})
export class HomeLayoutComponent implements OnInit {
  customer : Customer;
  constructor(private authService:AuthService) {
    this.customer = JSON.parse(localStorage.getItem('currentUser'));
    if(this.customer==null)
      this.authService.logout();
   }

  ngOnInit() {
    
    
  }

}
