import { Component, OnInit } from '@angular/core';
import {Customer} from '../customer';
import{AddCustomerService} from './add-customer.service';
import { ResponseBody } from '../responseBody';
import { Router } from "@angular/router";
import { isNull } from 'util';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  customer = new Customer();
  resp = new ResponseBody();
  message: string; 
  success : boolean;
  constructor(private addCustomerService : AddCustomerService,private router: Router)  { }

  ngOnInit() {
  }
   
  onClickSubmit(): void{ 
   // var pattern = new RegExp('\\w+(?:[.-]\\w+)*@\\w+(?:[.-]\\w+)*(?:\\.\\w{2,3})+', 'g');
   if(this.customer && (Object.keys(this.customer).length === 0)){
    this.message = "please enter all user information";
       
    this.success = false;
    return;
   }
    if(this.customer.address == null || this.customer.city == null || this.customer.email == null ||
      this.customer.firstName == null || this.customer.lastName == null || this.customer.mobileNo == null || 
      this.customer.password == null || this.customer.state == null || this.customer.username == null || this.customer.zip ==null ){
        this.message = "please enter all user information";
       
        this.success = false;
        return;
      }
     /* if(pattern.exec(this.customer.email)){
        this.message = "please enter valid email id";
        this.success = false;
        return;
      }*/
         this.addCustomerService.addCustomer(this.customer)
              .subscribe(customerData => 
                {this.resp = customerData;this.message = this.resp.message;
                  this.success = this.resp.code=='200' ? true : false;
                  if(this.resp.code == "200"){
                    localStorage.setItem('currentUser', JSON.stringify(this.customer));
                    this.router.navigate(['/main/getBal']);
                  }
               }
                ,(error)=>{console.log(error)
              });

        
  
}

}
