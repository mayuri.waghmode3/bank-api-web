import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Customer } from '../customer';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class AddCustomerService {

  constructor( private http: HttpClient) { }
  url : string;
  addCustomer(customer : Customer) : Observable<any>{
    let body = JSON.stringify(customer);
    this.url =  "http://localhost:8080/signup/";
    return this.http.post(this.url, body,httpOptions);
  
  }
}