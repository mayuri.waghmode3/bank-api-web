import { Customer } from '../customer';
import { AccountType } from './AccountType';

export class Account{
    accNo : string;
    balance : number;
    customer : Customer
    accountType : AccountType
}