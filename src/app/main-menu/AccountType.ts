export class AccountType{
    typeId : string;
    type   : string;
    description : string;
    interest_rate   : number;
}
