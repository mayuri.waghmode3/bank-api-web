import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Account } from './Account';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class MainServiceService {

  constructor(private http: HttpClient) { }
  url : string;
  getAccounts(username : string): Observable<Account[]> {
      this.url =  "http://localhost:8080/getAccounts/"+username;
      return this.http.get<Account[]>(this.url).pipe(catchError(this.handleError));
    }
  
    getBalance(accNo : string){
      console.log(accNo);
      this.url =  "http://localhost:8080/getBalance/"+accNo;
      return this.http.get<any>(this.url).pipe(catchError(this.handleError));
    }
    private handleError(error : Response){
      return Observable.throw(error);
    }
  
  }
  

