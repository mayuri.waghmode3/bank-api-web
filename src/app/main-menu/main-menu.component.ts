import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { MainServiceService } from './main-service.service';
import { Account } from './Account';
import { Observable } from 'rxjs';
import { ResponseBody } from '../responseBody';
import { AuthService } from '../login/auth.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {
  customer : Customer;
  accounts : Account[];
  mainAcc:string;
  success : boolean;
  accNo : string; 
  resp = new ResponseBody();
  constructor(private mainServiceService : MainServiceService) {
      this.customer = JSON.parse(localStorage.getItem('currentUser'));
      this.getAccounts();
    }

  ngOnInit() {
  }
  
  getBalance() {
    this.accNo = this.mainAcc.split("::")[0];
    this.mainServiceService.getBalance(this.accNo)
          .subscribe(data =>{this.resp = data; this.success = this.resp.code=='200' ? true : false;});
  }
  getAccounts() : void{
    this.mainServiceService.getAccounts(this.customer.username)
          .subscribe(accData => 
            {this.accounts = accData; this.mainAcc = this.accounts[0].accNo+"::"+this.accounts[0].accountType.type;}
            ,(error)=>{console.log(error)
          });
  }
}
