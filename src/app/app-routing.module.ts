import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';

import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { MainMenuComponent } from './main-menu/main-menu.component';
import { AppComponent } from './app.component';
import { WithdrawOpComponent } from './withdraw-op/withdraw-op.component';
import { DepositOpComponent } from './deposit-op/deposit-op.component';
import { TransferOpComponent } from './transfer-op/transfer-op.component';
import { ViewTransactionOpComponent } from './view-transaction-op/view-transaction-op.component';
import { AddAccOpComponent } from './add-acc-op/add-acc-op.component';
import { RemoveAccOpComponent } from './remove-acc-op/remove-acc-op.component';
import { EditUserOpComponent } from './edit-user-op/edit-user-op.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
const routes: Routes = [
  { path: '', redirectTo: 'login', data: { title: 'Login Form' }, pathMatch: 'full' },
  { path: 'login', component: LoginLayoutComponent, data: {title: 'Login Form'},
    children: [
      {path: '', component: LoginComponent}
    ]
  },
  { path: 'register', component: RegisterComponent },
  { path: 'main', component: HomeLayoutComponent,
  children: [
    { path: 'withdraw', component: WithdrawOpComponent },
    { path: 'deposit', component: DepositOpComponent },
    { path: 'transfer', component: TransferOpComponent },
    { path: 'view', component: ViewTransactionOpComponent },
    { path: 'addAcc', component: AddAccOpComponent },
    { path: 'removeAcc', component: RemoveAccOpComponent },
    { path: 'editUser', component: EditUserOpComponent },
    { path: 'getBal', component: MainMenuComponent },
  ]},
   // otherwise redirect to Pagenotfound
   { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
