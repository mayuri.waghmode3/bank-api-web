import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { GetCustomerService } from './login/get-customer.service';
import { RegisterComponent } from './register/register.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { AuthService } from './login/auth.service';
import { WithdrawOpComponent } from './withdraw-op/withdraw-op.component';
import { DepositOpComponent } from './deposit-op/deposit-op.component';
import { TransferOpComponent } from './transfer-op/transfer-op.component';
import { AddAccOpComponent } from './add-acc-op/add-acc-op.component';
import { RemoveAccOpComponent } from './remove-acc-op/remove-acc-op.component';
import { ViewTransactionOpComponent } from './view-transaction-op/view-transaction-op.component';
import { EditUserOpComponent } from './edit-user-op/edit-user-op.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { HomeLayoutComponent } from './home-layout/home-layout.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    PageNotFoundComponent,
    MainMenuComponent,
    NavMenuComponent,
    WithdrawOpComponent,
    DepositOpComponent,
    TransferOpComponent,
    AddAccOpComponent,
    RemoveAccOpComponent,
    ViewTransactionOpComponent,
    EditUserOpComponent,
    LoginLayoutComponent,
    HomeLayoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule

  ],
  providers: [GetCustomerService,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
